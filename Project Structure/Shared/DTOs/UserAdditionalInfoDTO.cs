﻿
namespace ProjectStructure.Shared.DTOs
{
    public class UserAdditionalInfoDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int TotalTasksCount { get; set; }
        public int TotalUncompletedAndCanceledTasks { get; set; }
        public TaskDTO LongestTask { get; set; }
    }
}