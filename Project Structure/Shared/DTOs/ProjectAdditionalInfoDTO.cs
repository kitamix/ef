﻿
namespace ProjectStructure.Shared.DTOs
{
    public class ProjectAdditionalInfoDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestTaskByDescription { get; set; }
        public TaskDTO ShortestTaskByName { get; set; }
        public int TotalTeamCount { get; set; }
    }
}