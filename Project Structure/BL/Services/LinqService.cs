﻿using AutoMapper;
using ProjectStructure.BL.Services.Abstract;
using ProjectStructure.Shared.DTOs;
using ProjectStructure.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace ProjectStructure.BL.Services
{
    public sealed class LinqService
    {
        private readonly ProjectStructureDbContext _context;
        private readonly IMapper _mapper;

        public LinqService
        (
            ProjectStructureDbContext context,
            IMapper mapper
        )
        {
            _context = context;
            _mapper = mapper;
        }
        public IDictionary<int, int> CountTasksOfCurrentUser(int userId)
        {
            var query = _context.Projects
                .Include(p => p.Tasks)
                .Where(p => p.AuthorId == userId)
                .ToDictionary(k => k.Id, v => v.Tasks.Count);
            return query;
        }
        public List<TaskDTO> GetTasksOfCurrentUser(int userId)
        {
            var query = _context.Tasks
                .Where(t => t.PerformerId == userId 
                            && t.Name.Length < 45)
                .ToList();
            return _mapper.Map<List<TaskDTO>>(query);
        }
        public List<Tuple<int, string>> GetFinishedThisYearTasksOfCurrentUser(int userId)
        {
            var query = _context.Tasks
                .Where(t => t.PerformerId == userId
                            && t.FinishedAt.HasValue
                            && t.FinishedAt.Value.Year == DateTime.Now.AddYears(-1).Year)
                .Select((task) => new Tuple<int, string>(task.Id, task.Name))
                .ToList();
            return query;
        }
        public IEnumerable<UsersOlderThanTenYearsOldDTO> GetUsersOlderThanTenYearsOld()
        {
            var query = _context.Teams
                .Include(t => t.Users)
                .Select(t => new UsersOlderThanTenYearsOldDTO
                {
                    Id = t.Id,
                    TeamName = t.Name,
                    UsersOlderThanTenYearsOld = _mapper
                        .Map<IEnumerable<UserDTO>>(t.Users.Where(u => DateTime.Now.Year - u.BirthDay.Year > 10)
                        .OrderByDescending(u => u.RegisteredAt)
                        .AsEnumerable())
                });
            return query;
        }
        public IEnumerable<SortedUserByAscendingAndTasksByDescending> GetSortedUserByAscendingAndTasksByDescending()
        {
            var query = _context.Users
                .Include(u => u.Tasks)
                .Select(u => new SortedUserByAscendingAndTasksByDescending
                {
                    UserName = u.FirstName,
                    SortedTasks = _mapper.Map<List<TaskDTO>>(u.Tasks
                        .OrderByDescending(t => t.Name.Length)
                        .ToList())
                })
                .OrderBy(u => u.UserName);
            return query;
        }
        public UserAdditionalInfoDTO AnalyzeUserProjectsAndTasks(int userId)
        {
            var query = _context.Users
                .Include(u => u.Projects)
                    .ThenInclude(p => p.Tasks)
                .Where(u => u.Id == userId)
                .Select(u => new UserAdditionalInfoDTO
                {
                    User = _mapper.Map<UserDTO>(u),
                    LastProject = _mapper.Map<ProjectDTO>(u.Projects
                            .OrderByDescending(p => p.CreatedAt)
                        .FirstOrDefault()),
                    TotalTasksCount = u.Projects
                        .OrderByDescending(p => p.CreatedAt)
                        .FirstOrDefault()
                        .Tasks
                        .Count,
                    TotalUncompletedAndCanceledTasks = u.Tasks
                        .Where(t => !t.FinishedAt.HasValue)
                        .Count(),
                    LongestTask = _mapper.Map<TaskDTO>(_context.Tasks.ToList()
                        .Where(t => t.PerformerId == userId 
                                    && t.FinishedAt.HasValue)
                        .OrderByDescending(t => t.FinishedAt.Value.Ticks - t.CreatedAt.Ticks)
                        .FirstOrDefault())
                })
                .First();
            return query;
        }
        public ProjectAdditionalInfoDTO AnalyzeProjectTasksAndTeam(int projectId)
        {
            var query = _context.Projects
                .Include(p => p.Tasks)
                .Include(p => p.Team)
                .Where(p => p.Id == projectId)
                .Select(p => new ProjectAdditionalInfoDTO
                {
                    Project = _mapper.Map<ProjectDTO>(p),
                    LongestTaskByDescription = _mapper.Map<TaskDTO>(p.Tasks
                        .OrderByDescending(t => t.Description.Length)
                        .FirstOrDefault()),
                    ShortestTaskByName = _mapper.Map<TaskDTO>(p.Tasks
                        .OrderBy(t => t.Name.Length)
                        .FirstOrDefault()),
                    TotalTeamCount = p.Description.Length > 20 || p.Tasks.Count < 3 ? p.Team.Users.Count : 0
                })
                .First();
            return query;
        }

    }
}