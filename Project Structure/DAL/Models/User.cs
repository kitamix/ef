﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectStructure.DAL.Models
{
    public class User
    {
        public int Id { get; set; }
        public Team Team { get; set; }
        public List<Task> Tasks { get; set; } = new List<Task>();
        public List<Project> Projects { get; set; } = new List<Project>();
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
    }
}