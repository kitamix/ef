﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProjectStructure.DAL.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [Required]
        public DateTime CreatedAt { get; set; }
        public List<User> Users { get; set; } = new List<User>();
    }
}