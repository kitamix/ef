﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Shared.DTOs;
using ProjectStructure.BL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly TeamService _teamService;

        public TeamsController(
            TeamService teamService
        )
        {
            _teamService = teamService;
        }

        [HttpPost]
        public async Task<TeamDTO> Create([FromBody] TeamDTO teamDTO)
        {
            return await _teamService.Create(teamDTO);
        }
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _teamService.Delete(id);
        }
        [HttpGet]
        public ICollection<TeamDTO> Get()
        {
            return _teamService.Get();
        }
        [HttpGet("{id}")]
        public TeamDTO Get(int id)
        {
            return _teamService.Get(id);
        }

        [HttpPut("{id}")]
        public async Task Update([FromBody] TeamDTO teamDTO)
        {
            await _teamService.Update(teamDTO);
        }
    }
}
